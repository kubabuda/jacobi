//============================================================================
// Name        : Jacobi.cpp
// Author      : J Buda
// Version     :
// Copyright   : MIT licence
// Description : Hello World in C++, Ansi-style
//============================================================================

/* Metoda Jacobiego
 * Jakub Buda
 * 2015
 */
#pragma once

#include "Jacobi.h"


using namespace std;
//using namespace Jacobi;


/*
    void get_b_from_terminal()
    {
    	int i;
	    for (i=0; i<num; i++) {
	    	cout << "b[" << i+1 << "] = ";
	    	b[i] = read_num();
	   }
    }

    void calc_N_D()
    {
    	int i, j;
    	// Calculate N = D^-1
    	for (i=0; i<num; i++)
    		N[i] = type(1) / A[i][i];

    	// Calculate M = -D^-1 (L + U)
    	for (i=0; i<num; i++)
    		for (j=0; j<num; j++)
    			if (i == j) {
    				M[i][j] = 0;
    			} else {
    				M[i][j] = type(0) - (A[i][j] * N[i]);
    			}
    	//Initialize x
    	for (i=0; i<num; i++){
    		x1[i] = 0;
    	}
    }

public:

    Jaco(int n) {
    	if(n > MAX_ARR) {
    		throw std::invalid_argument( "received negative value" );
    	}
    	num = n;
    	iter_cycles = n;
    }

    void iterate(void) {
    	int i, j, k;
    	cout << "Jacobi algorithm on n=" << num << endl;
    	this->get_A_from_terminal();
    	this->get_b_from_terminal();
    	this->calc_N_D();

    	printf("Ile iteracji algorytmu wykonac?\n");
	    scanf("%d", &iter_cycles);

	    for (k=0; k<iter_cycles; k++) {
		  for (i=0; i<num; i++) {
			 x2[i] = N[i]*b[i];
			 for (j=0; j<num; j++)
				x2[i] += M[i][j]*x1[j];
		  }
		  for (i=0; i<num; i++)
			 x1[i] = x2[i];
	   }

	   cout << "Wynik\n";
	   for (i=0; i<num; i++)
		  cout << "x[" << i+1 << "] = " << x1[i] << "\n";
    }

    int size()
    {
        return num;
    }
};
*/
