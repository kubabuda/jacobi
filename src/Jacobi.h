/*
 * Jacobi.h
 *
 *  Created on: 17 cze 2015
 *      Author: kuba
 */

#pragma once

#include <iostream>
#include <math.h>
#include <stdexcept>
#include <stdio.h>
#include <string>
#include <sstream>

#include "Number.h"
#include "Interval.h"
#define MAX_ARR 100


//namespace Jacobi {

template < class type >
class Jaco
{
private:
    type A[MAX_ARR][MAX_ARR];
	type M[MAX_ARR][MAX_ARR];
	type N[MAX_ARR];
	type b[MAX_ARR];
	type x1[MAX_ARR];
	type x2[MAX_ARR];

	int num = 0;
	int iter_cycles = 0;

	bool is_setup = false;

	void get_A_from_terminal(int p);

	type read_num(void)
	{
		int m;
		std::cin >> m;

		return type(m);
	}

    void get_A_from_terminal()
    {
		int i, j;
		for (i=0; i<num; i++){
			for (j=0; j<num; j++) {
			std::cout << "A[" << i + 1 << "][" << (j+1) << "] = ";
			A[i][j] = read_num();
			if ((i == j) && (A[i][j] == type(0))){
				while(A[i][j] == type(0)){
					std::cout << "Wartosci na przekatnej musza byc rozne od 0" << "\n";
					throw std::invalid_argument( "Wartości na przekatnej musza byc rozne od 0" );
					std::cout << "A[" << i + 1 << "][" << (j+1) << "] = ";
					A[i][j] = read_num();
			   }
			 }
		  }
       }
    }

    void get_b_from_terminal()
    {
    	int i;
	    for (i=0; i<num; i++) {
	    	std::cout << "b[" << i+1 << "] = ";
	    	b[i] = read_num();
	   }
    }


    void calc_N_D()
    {
    	int i, j;
    	// Calculate N = D^-1
    	for (i=0; i<num; i++)
    		N[i] = type(1) / A[i][i];

    	// Calculate M = -D^-1 (L + U)
    	for (i=0; i<num; i++) {
    		for (j=0; j<num; j++) {
    			if (i == j) {
    				M[i][j] = type(0);
    			} else {
    				M[i][j] = type(0) - (A[i][j] * N[i]);
    			}
    		}
    	}
    	//Initialize x
    	for (i=0; i<num; i++){
    		x1[i] = type(0);
    	}
    }


public:

    Jaco(int n) {
    	if(n > MAX_ARR) {
    		throw std::invalid_argument( "Wartość n zbyt duza dla obecnej wersji" );
    	}
    	num = n;
    	iter_cycles = n;
    }

    Jaco (int n, type args[]) {
    	if(n > MAX_ARR) {
    		throw std::invalid_argument( "Wartość n zbyt duza dla obecnej wersji" );
    	}
    	num = n;
    	iter_cycles = n;

    	load_from_array(args, n);

    }

    void setup() {
    	std::cout << "Jacobi algorithm on n=" << num << std::endl;
    	get_A_from_terminal();
    	get_b_from_terminal();
    	calc_N_D();
    	is_setup = true;
    }

    void iterate(void) {
    	if (!is_setup) {
        		setup();
        }

        std::cout <<"Ile iteracji algorytmu wykonac?\n";
        std::cin >> iter_cycles;
        iterate(iter_cycles);
    }

    void iterate(int cycles) {

    	int i, j, k;

	    for (k=0; k<cycles; k++) {
		  for (i=0; i<num; i++) {
			 x2[i] = N[i]*b[i];
			 for (j=0; j<num; j++)
				x2[i] += M[i][j]*x1[j];
		  }
		  for (i=0; i<num; i++)
			 x1[i] = x2[i];
	   }
	    std::cout << "Wynik\n";
	    for (i=0; i<num; i++)
	    	std::cout << "x" << i+1 << " = " << x1[i] << "\n";
    }

    void load_from_array(type args[], int args_nr)
	{
		if(num > args_nr) {
			throw std::invalid_argument( "Zbyt mało parametrów dla danego n" );
		}
		/* Load A, b from list
		 * [a,b,c,d,e,f] -> A=|a,c|;  b=|e|
		 * 					  |b,d|		|f|
		 */
		int k = 0;
		/* load A */
		for (int i=0; i<num; i++){
			for (int j=0; j<num; j++) {
				A[i][j] = args[k];
				++k;
				if ((i == j) && (A[i][j] == type(0))){
					print_self();
					std::cout << "[i,j=" << i << j <<"] val=" << A[i][j];
					throw std::invalid_argument( "Podano 0 na przekątnej macierzy" );
				}
			}
		}
		/* load b */
		for (int i = 0; i < num; i++){
			b[i] = args[k];
			++k;
		}
		calc_N_D();
		is_setup = true;
	}

    void print_self(void){
    	for (int i=0; i<num; i++) {
    		std::cout << "|";
    	    for (int j=0; j<num; j++) {
    	    	std::cout << A[i][j] << "\t";
    	    }
    	    std::cout << "|\t|" << b[i] << "|\t\t[" << x1[i] << "]\n" ;
    	}
    }

    int size()
    {
        return num;
    }
};
//} //namespace Jacobi
