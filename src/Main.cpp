#include <iostream>
#include <sstream>
#include <string>

#include "Jacobi.h"
#include "Number.h"
#include "Interval.h"

using namespace std;


int main(int argc, char *argv[])
{
	/* dummy sample data */
	int n = 2;
	char *sample_args[] = {"1.0", "0.3", "0.2", "13.0", "2.0", "3.4"};
	char **args = sample_args;
	int args_n = sizeof(sample_args) / sizeof(sample_args[0]);

	if (argc > 3) {
		/* loading data from argv:
		 * n, a00, a 01 .. a0n, a10, a11...a1n...an0, an1..ann, b0, b1..bn
		 */
		cout << "loading data from argv\n";
		n = std::stoi(argv[1]);
		args_n = n * (n + 1);
		args = (&argv[2]);  /* ignore argv[1] */
		cout << n << " | " << args[0] << "\n";

		if (argc <= (args_n + 1)) {
			cout << "Zbyt mało parametrów dla podanego układu z" << n << "niewiadomymi. Jest";
			cout << argc-1 << ", powinno być " << args_n + 1 << "\n";
			return 1;
		}
	} else {
		std::cout << argc << " arguments, demo:\n";
	}

	float floats[args_n];
	ean::Extended exts[args_n];
	ean::Interval inter[args_n];

	for(int i=0; i<args_n; ++i){
		exts[i] = ean::Extended(args[i]);
		inter[i] = ean::Interval(args[i]);

			floats[i] = std::stof(args[i]);
	}

	Jaco < float > FppArithm(n);
	Jaco < ean::Extended > ExtArithm(n);
	Jaco < ean::Interval > InterArithm(n);

	FppArithm.load_from_array(floats, args_n);
	ExtArithm.load_from_array(exts, args_n);
	InterArithm.load_from_array(inter, args_n);

	/* Calculate */
	std::cout << "\nFloating point arithmetic\n";
	FppArithm.iterate(args_n);
	std::cout << "\nExtended floating point arithmetic\n";
	ExtArithm.iterate(args_n);
	std::cout << "\nInterval arithmetic\n";
	InterArithm.iterate(args_n);

	return 0;
}
