#!/usr/bin/python
# -*- coding: utf-8 -*-

from time import sleep
from ttk import Frame, Label
from Tkinter import Tk, Button, BOTH, Listbox, StringVar, END
import tkFileDialog
import os
from _tkinter import TclError


log_filename = "res.log"


class JacobiGUI(Frame):

    def __init__(self, parent):
        Frame.__init__(self, parent)

        self.parent = parent
        self.initUI()

    def initUI(self):
        self.parent.title("Metoda Jacobiego")

        self.pack(fill=BOTH, expand=1)
        self.loadLi([])

        ot = Button(self, text="Otwórz", command=self.onOpenFile)
        ot.place(x=200, y=20)

        self.var = StringVar()
        self.label = Label(self, text=0, textvariable=self.var)
        self.label.place(x=20, y=210)

    def loadLi(self, results):
        lb = Listbox(self)

        for i in results:
            lb.insert(END, i)

        lb.bind("<<ListboxSelect>>", self.onSelect)
        lb.place(x=20, y=20)

    def onCalculate(self):
        args = " ".join(self.input_data)
        command = "./Debug/Jacobi " + args + " > " + log_filename
        os.system(command)
        results = []
        tries = 0
        sleep(1)
        while not results and tries < 10:
            tries += 1
            try:
                with open(log_filename, "r") as log_file:
                    log_file = log_file.readlines()
                    self.var.set("Obliczono")
                    results = log_file[3:]
            except IOError:
                self.var.set("Wystąpił błąd działania programu")
        if "exception" in log_file:
            self.var.set("Wystąpił błąd, zarejestrowano w " + log_filename)
            return
        else:
            results = [" ".join(i.rsplit()) for i in results]
            self.loadLi(results)

    def onSelect(self, val):

        sender = val.widget
        idx = sender.curselection()
        try:
            value = sender.get(idx)
            self.var.set(value)
        except TclError:
            pass    # empty listbox with "" index

    def onOpenFile(self):
        f_types = [("Text files", "*.txt"), ("All files", "*")]
        dialog = tkFileDialog.Open(self, filetypes=f_types)
        name = dialog.show()
        if name:
            try:
                data = self.readFile(name)
            except IOError:
                self.var.set(u"Wystąpił błąd przy otwieraniu pliku")
            else:
                self.var.set(data)
                do = Button(self, text="Oblicz", command=self.onCalculate)
                do.place(x=200, y=60)
                self.var.set("Wybrano " + name)
        else:
            self.var.set("Nie wybrano pliku")

    def readFile(self, name):
        with open(name, "r") as input_file:
            self.input_data = [i.strip() for i in input_file.readlines() if i]
            self.loadLi(self.input_data)
            self.var.set(u"Poprawnie załadowano dane z pliku:" + name)
        return name


def main():
    root = Tk()
    ex = JacobiGUI(root)
    root.geometry("300x250+300+300")
    root.mainloop()


if __name__ == '__main__':
    main()
